# DSMR reader

This script reads out the variables obtained by DSMR (Dutch Smart Meter Reader).

# Docker-compose
First, a docker-compose containing the settings for ualex73/dsmr-reader-docker is ran.
From the web application one then has to enable the api and retrieve the api key.

# main.py
The main.py file then uses all the variables in the projectsecrets.py file containing all the regular
secrets, but also including the api key retrieved from the web application.
The script is then ran as a cronjob every minute.
