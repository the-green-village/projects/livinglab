import requests
import pandas as pd

from tgvfunctions import tgvfunctions
from projectsecrets import DSMR_API, URL, DEVICE_ID
from datetime import datetime, timedelta

# Specify begin- and endtime, the minute offset is because data is not _always_ reported realtime so sometimes a measurement is missed

endtime = tgvfunctions.roundTime(roundTo=60) - timedelta(minutes=1)
begintime = endtime - timedelta(minutes=1)

headers = {
        "X-AUTHKEY": DSMR_API,
        "content-type": "application/json",
        }

params = {
        "timestamp__gte": begintime,
        "timestamp__lte": endtime,
        }

response = requests.get(URL, headers=headers, params=params)
data = pd.DataFrame(response.json()["results"])
data["timestampepochms"] = pd.to_datetime(data["timestamp"], infer_datetime_format=True)
data["timestampepochms"] = data["timestampepochms"].astype("int64")//1e6
producer = tgvfunctions.makeproducer('livinglab-producer')

# Determine the unit based on the description
units = dict()
for col in data.columns:
    if "currently" in col:
        units[col] = "kW"
    elif "electricity" in col:
        units[col] = "kWh"
    elif "voltage" in col:
        units[col] = "V"
    elif "current_" in col:
        units[col] = "A"
    else:
        # In case a unit is not found, it should not be assigned a wrong one (such as A), therefore it should be None
        units[col] = None


for idx, row in data.iterrows():
    # All columns instead of id, timestamp, and timestampepochms
    for col in data.columns[2:-1]:
        measurement_description = str(DEVICE_ID + " " + col)
        value = {
                        "project_id": "livinglab",
                        "application_id": "dsmr",
                        "device_id": DEVICE_ID,
                        "timestamp": int(row["timestampepochms"]),
                        "measurements": [
                            {
                                "measurement_id": measurement_description,
                                "measurement_description": measurement_description,
                                "value": float(row[col]),
                                "unit": units[col]
                            }
                        ]
                    }
        tgvfunctions.produce(producer, value)
producer.flush()
